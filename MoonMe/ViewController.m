//
//  ViewController.m
//  MoonMe
//
//  Created by Alex Brown on 1/12/16.
//  Copyright © 2016 PixelNinja. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    BackGroundGradientView * bggv = [[BackGroundGradientView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:bggv];

    backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
    backgroundView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backgroundView];
    
    titleLbl = [[UILabel alloc] initWithFrame:CGRectMake(25, 150, self.view.frame.size.width - 200, 80)];
    titleLbl.textColor = [UIColor grayColor];
    titleLbl.textAlignment = NSTextAlignmentCenter;
    titleLbl.text = @"Project";
    titleLbl.font = [titleLbl.font fontWithSize:25];
    [backgroundView addSubview:titleLbl];
    
    title2Lbl = [[UILabel alloc] initWithFrame:CGRectMake(100, 180, self.view.frame.size.width - 100, 80)];
    title2Lbl.textColor = [UIColor grayColor];
    title2Lbl.textAlignment = NSTextAlignmentCenter;
    title2Lbl.text = @"BECOMING";
    title2Lbl.font = [title2Lbl.font fontWithSize:50];
    title2Lbl.alpha = 0;
    [backgroundView addSubview:title2Lbl];
    
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(timer:)
                                   userInfo:nil
                                    repeats:NO];

    beginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [beginBtn addTarget:self
               action:@selector(beginAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [beginBtn setTitle:@"Begin" forState:UIControlStateNormal];
    beginBtn.frame = CGRectMake(100.0, 600.0, self.view.frame.size.width - 200, 40.0);
    [backgroundView addSubview:beginBtn];
    
}

//NSTimer
- (void)timer: (id) sender {
    
    title2Lbl.alpha = 0;
    
    [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{ title2Lbl.alpha = 1;}
                     completion:nil];
    
}

//Begin button action
- (void)beginAction: (id) sender {
    
    CommandViewController * viewController = [[CommandViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
