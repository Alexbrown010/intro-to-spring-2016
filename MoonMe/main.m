//
//  main.m
//  MoonMe
//
//  Created by Alex Brown on 1/12/16.
//  Copyright © 2016 PixelNinja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
