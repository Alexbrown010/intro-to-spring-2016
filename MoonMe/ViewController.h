//
//  ViewController.h
//  MoonMe
//
//  Created by Alex Brown on 1/12/16.
//  Copyright © 2016 PixelNinja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
    //UIViews
    UIView * backgroundView;
    
    //UILabels
    UILabel * titleLbl;
    UILabel * title2Lbl;
    
    //UIButtons
    UIButton * beginBtn;
    
}


@end

