//
//  CommandViewController.m
//  MoonMe
//
//  Created by Alex Brown on 1/12/16.
//  Copyright © 2016 PixelNinja. All rights reserved.
//

#import "CommandViewController.h"

@interface CommandViewController ()

@end

@implementation CommandViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    clearView = [[UIView alloc] initWithFrame:self.view.bounds];
    clearView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:clearView];
    
    menuView = [[UIView alloc] initWithFrame:CGRectMake(10, 550, self.view.frame.size.width - 20, 80)];
    menuView.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    [clearView addSubview:menuView];
    
    healthBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [healthBtn addTarget:self
                 action:@selector(healthAction:)
       forControlEvents:UIControlEventTouchUpInside];
    [healthBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [healthBtn setTitle:@"Health" forState:UIControlStateNormal];
    healthBtn.frame = CGRectMake(10.0, 0.0, 50, 40.0);
    [healthBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:healthBtn];
    
    recipesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [recipesBtn addTarget:self
                  action:@selector(healthAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [recipesBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [recipesBtn setTitle:@"Recipes" forState:UIControlStateNormal];
    recipesBtn.frame = CGRectMake(65.0, 0.0, 50, 40.0);
    [recipesBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:recipesBtn];
    
    journalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [journalBtn addTarget:self
                  action:@selector(healthAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [journalBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [journalBtn setTitle:@"Journal" forState:UIControlStateNormal];
    journalBtn.frame = CGRectMake(120.0, 0.0, 50, 40.0);
    [journalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:journalBtn];
    
    exerciseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [exerciseBtn addTarget:self
                  action:@selector(healthAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [exerciseBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [exerciseBtn setTitle:@"Exercise" forState:UIControlStateNormal];
    exerciseBtn.frame = CGRectMake(175.0, 0.0, 50, 40.0);
    [exerciseBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:exerciseBtn];
    
    socialMediaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [socialMediaBtn addTarget:self
                  action:@selector(healthAction:)
        forControlEvents:UIControlEventTouchUpInside];
    [socialMediaBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [socialMediaBtn setTitle:@"Social" forState:UIControlStateNormal];
    socialMediaBtn.frame = CGRectMake(230.0, 0.0, 50, 40.0);
    [socialMediaBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:socialMediaBtn];
    
    ritualBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ritualBtn addTarget:self
                       action:@selector(healthAction:)
             forControlEvents:UIControlEventTouchUpInside];
    [ritualBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [ritualBtn setTitle:@"Ritual" forState:UIControlStateNormal];
    ritualBtn.frame = CGRectMake(285.0, 0.0, 50, 40.0);
    [ritualBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuView addSubview:ritualBtn];
    
}

- (void)healthAction: (id) sender {
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
