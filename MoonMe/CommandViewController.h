//
//  CommandViewController.h
//  MoonMe
//
//  Created by Alex Brown on 1/12/16.
//  Copyright © 2016 PixelNinja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommandViewController : UIViewController {
    
    //UIViews
    UIView * clearView;
    UIView * menuView;
    
    //UIViewImageView
    UIImageView * images;
    
    //UIScrollView
    UIScrollView * scrollView;
    
    //UIButton
    UIButton * calendarBtn;
    UIButton * healthBtn;
    UIButton * recipesBtn;
    UIButton * journalBtn;
    UIButton * exerciseBtn;
    UIButton * socialMediaBtn;
    UIButton * ritualBtn;
    UIButton * moonBtn;
    
    
    
}

@end
